class Code
  attr_reader :pegs
  PEGS = {"R" => "Red", "Y" => "Yellow", "B" => "Blue", "G" => "Green", "O"=> "Orange", "P"=>"Purple"}

  def initialize (pegs)
    raise unless pegs.is_a? Array
    @pegs = pegs
  end

  def self.parse(code)
    ncode = code.upcase.delete(",").split("")
    ncode.each do |color|
      raise if !(PEGS.keys.include?(color))
    end
    Code.new(ncode)
  end

  def self.random
    pegs = []
    4.times {pegs << PEGS.keys[rand(0..5)]}
    Code.new(pegs)
  end

  def [](pos)
    @pegs[pos]
  end

  def exact_matches(guess)
    exact = 0
    guess.pegs.each_with_index do |peg, idx|
      exact+=1 if peg == @pegs[idx]
    end
    exact
  end

  def near_matches(guess)
    temp = @pegs
    near = 0
    guess.pegs.each_with_index do |peg, idx|
      if temp.include?(peg) && peg!=@pegs[idx]
        near+=1
        temp.slice!(temp.index(peg))
      end
    end
    near
  end

  def ==(guess)
    return false if !(guess.is_a?(Code))
    self.exact_matches(guess) ==4
  end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code= Code.random)
    @secret_code = secret_code

  end

  def get_guess
    puts "Guess the code: "
    guess = $stdin.gets.chomp
    Code.parse(guess)
  end

  def display_matches(code)
    puts "exact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

end
